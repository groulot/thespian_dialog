from __future__ import annotations

import uuid
from typing import Mapping, Type, Tuple

python_type = type


class DialogMessage:
    def __init__(self):
        self.ask_id = uuid.uuid4().int


class QuestionBase(DialogMessage):
    answers = ()


def question(*, answers: Tuple[Type[DialogMessage], ...]):
    def decorator(klass):
        assert issubclass(klass, QuestionBase)
        klass.answers = answers
        return klass

    return decorator
