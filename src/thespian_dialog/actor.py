import uuid
from types import GeneratorType
from typing import Tuple, Optional, Dict

from thespian.actors import ActorTypeDispatcher, ActorAddress, PoisonMessage

from .message import DialogMessage, QuestionBase


class Ask:
    """
    Sends a question for an actor from a message handler. Returns the answer.

    This class is to be used to send a question to another actor and receive an
    answer without breaking the program flow. Requires that your actor derives from
    DialoguingActor.

    Example:
        def receiveMsg_whatever(self, message, sender):
        ...
        the_answer = yield Ask(recipient=deep_thought, message="What is the answer to Life?")
        print(the_answer)
        ...
    """
    def __init__(self, *, recipient: ActorAddress, message: DialogMessage):
        assert type(recipient) == ActorAddress
        self.recipient = recipient
        assert isinstance(message, DialogMessage)
        self.message = message


class DialoguingActor(ActorTypeDispatcher):
    """
    This ActorTypeDispatcher subclass with added capabilities for dialogs.

    This Actor subclass uses coroutines to save the context of your message handler and
    allows for a more streamlined program flow.
    """
    def __init__(self, *args, **kwargs):
        super().__init__()
        self.coroutines: Dict[int, GeneratorType] = {}
        self.coroutines_questions: Dict[GeneratorType, Tuple[ActorAddress, QuestionBase]] = {}
        self.current_question: Optional[Tuple[ActorAddress, QuestionBase]] = None

    @staticmethod
    def new_message_id():
        return uuid.uuid4()

    def send(self, recipient, message):
        """
        sends message to another actor with added checks to prevent it's erroneous use to
        reply to a question.
        """
        if self.current_question is not None:
            (question_sender, question) = self.current_question
            if recipient == question_sender and isinstance(message, question.answers):
                raise(Exception("use reply to reply to questions"))
        super().send(recipient, message)

    def reply(self, recipient, message):
        """
        Sends a reply to a question.

        The combination of recipient and message class must be correct: recipient must be the original
        sender of the question and the type of the message must be included in the allowed types declared
        for that question with the @question decorator.
        :param recipient: The recipient.
        :param message: The message. Type must be in acceptable answers type for the question.
        """
        assert isinstance(message, DialogMessage)
        if self.current_question is not None:
            (question_sender, question) = self.current_question
            if recipient == question_sender:
                if isinstance(message, question.answers):
                    message.ask_id = question.ask_id
                    super().send(recipient, message)
                else:
                    raise (Exception("Wrong answer to question"))
        else:
            raise (Exception("Replying to no question"))

    def reply_to(self, orig_message, recipient, message):
        """
        Reply to a message.

        Does not check message type and recipient. Easier but more error prone.
        :param orig_message: the original message this is a reply to. Must be a subclass of DialogMessage.
        :param recipient: the recipient.
        :param message: the message. Must be a subclass of DialogMessage.
        :return:
        """
        assert isinstance(orig_message, DialogMessage)
        assert isinstance(message, DialogMessage)
        message.ask_id = orig_message.ask_id
        self.send(recipient, message)

    def receiveMessage(self, message, sender):
        """
        Message handler wrapper with added logic to handle requests and context saving with coroutines.
        """
        # Handle responses
        if type(message) == PoisonMessage:
            if hasattr(message.poisonMessage, 'ask_id') and message.poisonMessage.ask_id in self.coroutines:
                # Copy ask_id to poison wrapper and resume processing
                message.ask_id = message.poisonMessage.ask_id
        if hasattr(message, 'ask_id') and message.ask_id in self.coroutines:
            # We received a response message for one of the saved coroutines
            coroutine = self.coroutines[message.ask_id]
            if coroutine in self.coroutines_questions:
                # Restore current question for the coroutine
                self.current_question = self.coroutines_questions[coroutine]
            del self.coroutines[message.ask_id]
            try:
                # Run the coroutine with the message
                coroutine_yield = coroutine.send(message)
                if isinstance(coroutine_yield, Ask):
                    # The coroutine sends a question to an actor
                    self.coroutines[coroutine_yield.message.ask_id] = coroutine
                    self.send(coroutine_yield.recipient, coroutine_yield.message)
            except StopIteration:
                # The message handler coroutine has finished. Cleanup.
                if coroutine in self.coroutines_questions:
                    del self.coroutines_questions[coroutine]
                pass
            self.current_question = None
        else:
            # This is not an answer to a question
            if isinstance(message, QuestionBase):
                # This is a question, note down needed info.
                self.current_question = (sender, message)
            # Handle the message.
            result = super().receiveMessage(message, sender)
            if isinstance(result, GeneratorType):
                # The handler is not a function but a coroutine.
                coroutine = result
                if isinstance(message, QuestionBase):
                    # Note down the question for later retrieval.
                    self.coroutines_questions[coroutine] = self.current_question
                try:
                    # Run the coroutine.
                    coroutine_yield = coroutine.send(None)
                    if isinstance(coroutine_yield, Ask):
                        # The coroutine sends a question to an actor
                        ask_request = coroutine_yield
                        self.coroutines[ask_request.message.ask_id] = coroutine
                        self.send(ask_request.recipient, ask_request.message)
                except StopIteration:
                    # The coroutine finished.
                    pass
                result = None
                self.current_question = None
            return result

