# This is a classic Thespian application to compute the number of calories in a
# recipe.
#
# Note: As is no concurrent request is possible to the recipe actor. Code would need to be added to
# track request ids to the database actor to do so, as well as a container for currently processed recipes
# in the calculator actor.

from thespian.actors import ActorTypeDispatcher, ActorSystem

from cooking import gramcalories, Ingredient, Recipe


class Response:
    def __init__(self, calories_per_gram):
        super(Response, self).__init__()
        self.calories_per_gram=calories_per_gram


class Query:
    def __init__(self, ingredient):
        self.ingredient = ingredient


class IngredientDB(ActorTypeDispatcher):
    def receiveMsg_Query(self, message:Query, sender):
        self.send(sender, Response(gramcalories[message.ingredient]))


class RecipeCaloriesCalculator(ActorTypeDispatcher):
    def db(self):
        return self.createActor(IngredientDB, globalName="db")

    def receiveMsg_Recipe(self, message, sender):
        # Store the recipe.
        self.recipe = message
        self.recipe_sender = sender
        self.calories = 0.0
        # start querying DB.
        self.current_ingredient = self.recipe.pop()
        self.send(self.db(), Query(self.current_ingredient.name))

    def receiveMsg_Response(self, message: Response, _sender):
        self.calories += message.calories_per_gram * self.current_ingredient.weight
        if len(self.recipe) > 0:
            self.current_ingredient = self.recipe.pop()
            self.send(self.db(), Query(self.current_ingredient.name))
        else:
            # We're done.
            self.send(self.recipe_sender, self.calories)

if __name__ == "__main__":
    asys = ActorSystem(systemBase='multiprocQueueBase')
    calculator = asys.createActor(RecipeCaloriesCalculator)
    calories = asys.ask(calculator, Recipe([
        Ingredient('sugar', 250),
        Ingredient('flour', 250),
        Ingredient('egg', 250),
        Ingredient('butter', 250)
    ]))
    print("Total calories: %s" % calories)
    asys.shutdown()
