gramcalories = {
    'sugar': 4.0,
    'flour': 3.6,
    'butter': 7.17,
    'egg': 0.96
}

class Ingredient:
    def __init__(self, name, weight):
        self.name = name
        self.weight = weight

class Recipe:
    def __init__(self, ingredients):
        self.ingredients = ingredients

    def __len__(self):
        return len(self.ingredients)

    def pop(self):
        return(self.ingredients.pop())