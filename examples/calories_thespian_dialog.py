# This is a Thespian-dialog application to compute the number of calories in a
# recipe.
#
# Note: As is, concurency is handled. Questions and their responses are tracked by ID behind the scene so
# no mixup is possible and all of the state needed to process a recipe is stored in the coroutine handler's
# local variables.

from thespian.actors import ActorSystem

from cooking import Recipe, Ingredient, gramcalories
from thespian_dialog import Ask, DialogMessage, DialoguingActor, question, QuestionBase


class Response(DialogMessage):
    def __init__(self, calories_per_gram):
        super(Response, self).__init__()
        self.calories_per_gram=calories_per_gram


@question(answers=(Response,))
class Query(QuestionBase):
    def __init__(self, ingredient):
        super(Query, self).__init__()
        self.ingredient = ingredient


class IngredientDB(DialoguingActor):
    def receiveMsg_Query(self, message: Query, sender):
        self.reply(sender, Response(gramcalories[message.ingredient]))


class RecipeCaloriesCalculator(DialoguingActor):
    def db(self):
        return self.createActor(IngredientDB, globalName="db")

    def receiveMsg_Recipe(self, message: Recipe, sender):
        # No need to store the recipe.
        calories = 0.0
        for ingredient in message.ingredients:
            # Query DB as you go.
            response = yield Ask(recipient=self.db(), message=Query(ingredient.name))
            calories += response.calories_per_gram * ingredient.weight
        # We're done.
        self.send(sender, calories)

if __name__ == "__main__":
    asys = ActorSystem(systemBase='multiprocQueueBase')
    calculator = asys.createActor(RecipeCaloriesCalculator)
    calories = asys.ask(calculator, Recipe([
        Ingredient('sugar', 250),
        Ingredient('flour', 250),
        Ingredient('egg', 250),
        Ingredient('butter', 250)
    ]))
    print("Total calories: %s" % calories)
    asys.shutdown()
