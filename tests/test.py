from __future__ import annotations

from functools import reduce
from unittest import TestCase
from unittest.mock import Mock

from thespian.actors import ActorTypeDispatcher, ActorSystem, ActorAddress, PoisonMessage

from thespian_dialog.actor import DialoguingActor, Ask
from thespian_dialog import DialogMessage, QuestionBase, question


class Eval:
    def __init__(self, path, args, kwargs):
        self.path = path
        self.args = args
        self.kwargs = kwargs

class MockActor(ActorTypeDispatcher):
    def receiveMsg_Eval(self, msg: Eval, sender):
        try:
            attr = reduce(lambda inst, item: inst.__getattribute__(item), msg.path, self)
            result = attr(*msg.args, **msg.kwargs)
            self.send(targetAddr=sender, msg=result)
        except Exception as ex:
            self.send(targetAddr=sender, msg=ex)

    def receiveUnrecognizedMessage(self, msg, sender):
        attr_name = 'receiveMsg_%s' % type(msg).__name__
        self.__setattr__(attr_name, Mock())
        self.__getattribute__(attr_name)(self, msg, sender)

class ActorProxy:
    def __init__(self, asys, actor, attribute_path = []):
        self.asys = asys
        self.actor = actor
        self.attribute_path = attribute_path

    def __getattr__(self, item):
        return ActorProxy(self.asys, self.actor, self.attribute_path + [item])

    def __call__(self, *args, **kwargs):
        result = self.asys.ask(self.actor, Eval(self.attribute_path, args, kwargs))
        if isinstance(result, Exception):
            raise result


class Response(DialogMessage):
    def __init__(self, response:str):
        assert type(response) == str
        super().__init__()
        self.response = response

@question(answers=(Response))
class Question(QuestionBase):
    def __init__(self, question:str):
        assert type(question) == str
        super().__init__()
        self.question = question

class QuestioningActor(DialoguingActor):
    def receiveMsg_ActorAddress(self, message: ActorAddress, sender:ActorAddress):
        assert type(message) == ActorAddress
        response:Response = yield Ask(recipient=message, message=Question("Life, the Universe and Everything"))
        if type(response) == PoisonMessage:
            self.send(recipient=sender, message="Got poison message as a response")
        else:
            self.send(recipient=sender, message="The reply was: %s" % response.response)

class WrongQuestioningActor(DialoguingActor):
    def receiveMsg_ActorAddress(self, message: ActorAddress, sender:ActorAddress):
        assert type(message) == ActorAddress
        try:
            response: Response = yield Ask(recipient=message, message="Life, the Universe and Everything")
            self.send(recipient=sender, message="The reply was: %s" % response.response)
        except AssertionError as ex:
            self.send(recipient=sender, message="AssertionError occured")

class AnsweringActor(DialoguingActor):
    def receiveMsg_Question(self, message: Question, sender: ActorAddress):
        assert type(message) == Question
        self.reply(sender, Response("The response to %s is 42" % message.question))

class WrongAnsweringActor(DialoguingActor):
    def receiveMsg_Question(self, message: Question, sender: ActorAddress):
        assert type(message) == Question
        self.reply(sender, "The response to %s is 42" % message.question)

class WrongAnsweringActor2(DialoguingActor):
    def receiveMsg_Question(self, message: Question, sender: ActorAddress):
        assert type(message) == Question
        self.reply(sender, DialogMessage("The response to %s is 42" % message.question))

class Test(TestCase):
    def test_ok(self):
        try:
            asys = ActorSystem(systemBase='multiprocQueueBase')
            questioning = asys.createActor(actorClass=QuestioningActor)
            wrongquestioning = asys.createActor(actorClass=WrongQuestioningActor)
            answering = asys.createActor(actorClass=AnsweringActor)
            wronganswering = asys.createActor(actorClass=WrongAnsweringActor)
            wronganswering2 = asys.createActor(actorClass=WrongAnsweringActor2)

            with self.subTest('send an incorrect messsage type as a question'):
                result=asys.ask(actorAddr=wrongquestioning, msg=answering)
                self.assertEqual(result, "AssertionError occured")

            with self.subTest('send an incorrect messsage type as a response'):
                result=asys.ask(actorAddr=questioning, msg=wronganswering)
                self.assertEqual(result, "Got poison message as a response")

            with self.subTest('send an incorrect response type as a response'):
                result=asys.ask(actorAddr=questioning, msg=wronganswering2)
                self.assertEqual(result, "Got poison message as a response")

            with self.subTest('reply to the question'):
                result = asys.ask(actorAddr=questioning, msg=answering)
                self.assertEqual(result, "The reply was: The response to Life, the Universe and Everything is 42")

        finally:
            asys.shutdown()