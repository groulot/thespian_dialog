# Thespian-dialog

This is an extension to [Thespian](https://thespianpy.com/) that adds question-response handling and  coroutine 
message handlers.

Thespian lets your main program send questions to actors with the "ask" function, but does 
not provide any such functionnality for communications between actors ; you are to 
code message and response tracking if needed.
Thespian-dialog provides a simple question-response functionnality via the use of 
unique ID attached to questions and their responses. 
It also introduce support for coroutine message handler for more comfort.

## Before

```python
def receiveMsg_str(self, message, sender):
    self.name = message
    self.original_sender = sender
    self.send(calendar_service, ("Some_id_1", "get_date"))

def receiveMsg_Date(self, message, _sender):
    (id, self.date) = message
    if id == "Some_id_1":
        self.send(weather_service, ("Some_id_2", "get_temperature"))

def receiveMsg_Temperature(self, message, _sender):
    (id, self.temperature) = message
    if id == "Some_id_2":
        self.send(quote_service, ("Some_id_3", "get_insightful_quote"))

def receiveMsg_Quote(self, message, _sender):
    (id, self.quote) = message
    if id == "Some_id_3":
        self.send(
            self.original_sender,
            "Hello {}, today is {}, the temperature is {}. Please meditate on this : {}".format(
                self.name, self.date, self.temperature, self.quote            
            )
        )    
```

## After

```python
def ReceiveMsg_str(self, message, sender):
    name = message
    date = yield Ask(calendar_service, "get_date"))
    temperature = yield Ask(weather_service, "get_temperature")
    quote = yield Ask(quote_service, "get_insightful_quote")
    self.send(
        sender,
        "Hello {}, today is {}, the temperature is {}. Please meditate on this : {}".format(
            name, date, temperature, quote            
        )
    )
```

See [example](example) for more comparisons.

# Word of warning

This is a toy library and has only been used for my personnal projects. 



